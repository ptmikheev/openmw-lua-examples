# OpenMW Lua examples

This repo contains several simple OpenMW mods.
They were developed as [OpenMW Lua](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/index.html) examples.

## UI modes

Various gameplay tweaks.

Features (each feature can be separately enabled or disabled in mod settings):
- Separate UI pages for Map, Spells, Stats, Journal
- Skyrim-like widget to choose the page (controls: mouse, movement keys, controller DPad)
- Key bindings 'M' (Map), 'C' (Stats), 'I' (Inventory)
- Attack button (left mouse click) automatically prepares the last used weapon/spell.
- An option to not pause the game in inventory and dialogs.
- Ability to rotate 3rd person camera in inventory mode and during dialogs (controls: left/right arrow keys, controller right stick).

### Installation

Download the mod and add the following lines to `openmw.cfg`:
```
# Specify here the path to the files
data="path/to/openmw-lua-examples/UiModes"

content=UiModes.omwscripts
```

## Advanced camera

Features (each feature can be separately enabled or disabled in the in-game settings menu):

- **Free camera** -- simply adds free camera mode.

    Controls:

    - Middle mouse button -- toggle free camera mode (don't forget to enable it in settings first)
    - Toggle POV (TAB by default) in free camera mode -- switch between *camera controls* or *player controls*
    - WASD -- move camera horizontally
    - Mouse wheel -- move camera vertically

- **Bow aiming** -- change field of view when aiming.
    The same effect is used in [Marksman's Eye](https://modding-openmw.gitlab.io/marksmans-eye/), but there it becomes available only after completing a quest.

**These features are now built-in in OpenMW, so removed from here:**

- ~~**Slow view change** -- makes switching from 1st person to 3rd person view not instant.~~

- ~~**First person auto switch** -- auto switch to 1st person view if there is an obstacle right behind the player.~~

- ~~**Move 360** -- Fallout 4 style of third person view. View direction and character direction become completely independent unless a weapon is used.~~

### Installation

Download the mod and add the following lines to `openmw.cfg`:
```
# Specify here the path to the files
data="path/to/openmw-lua-examples/AdvancedCamera"

content=AdvancedCamera.omwscripts
```

## OpenNevermind

Turns OpenMW into an engine for isometric RPGs.
Was developed as a [1st April joke](https://openmw.org/2022/openmw-roadmap-update/), but surprisingly
it turned out to be actually playable and some people liked it. So now OpenNevermind is available as a mod.

Not compatible with "Advanced camera".

### Controls

- Left mouse click -- move to clicked ground or interact with clicked object
- Mouse wheel -- scroll to change camera zoom
- `[W]`, `[S]` -- control camera pitch
- `[A]`, `[D]` -- control camera yaw
- `[TAB]` -- switch camera modes *Classic first person* / *Top down view* / *Top down view with camera collisions* (key binding is equal to `<Toggle POV>` in the settings menu)

The game starts with first person view (otherwise it is not convenient to do the intro quest), can be switched (`[TAB]`) to top down view when the character generation is finished.

### Installation

Download the mod and add the following lines to `openmw.cfg`:
```
# Specify here the path to the files
data="path/to/openmw-lua-examples/OpenNevermind"

content=OpenNevermind.omwscripts
```

## AI tweaks examples

**Angry monsters**

Makes rats and cliff racers attack not only the player, but every NPC they see (distance < 800).
So if you are trying to escape a pack of cliff racers, just run directly to the nearest bandit camp...

In order to add other monsters, edit the list in `AiTweaksExamples/angry_monsters/global.lua`.

**Enemies use doors**

NPCs can use teleport doors and chase the player.

**Installation**

Download the mod and add the following lines to `openmw.cfg`:
```
# Specify here the path to the files
data="path/to/openmw-lua-examples/AiTweaksExamples"

content=angry_monsters.omwscripts
content=enemies_use_doors.omwscripts
```

## Skooma effect

Affects camera and player movement after drinking skooma.

**Installation**

Download the mod and add the following lines to `openmw.cfg`:
```
# Specify here the path to the files
data="path/to/openmw-lua-examples/SkoomaEffect"

content=skooma_effect.omwscripts
```

## Fast equip

Provides hotkeys to switch between several equipment sets.

Controls:

- 'M' -- save current equipment
- '<', '>' -- iterate over saved equipment sets
- '/' -- remove current equipment set from the list

**Installation**

Download the mod and add the following lines to `openmw.cfg`:
```
# Specify here the path to the files
data="path/to/openmw-lua-examples/FastEquip"

content=fast_equip.omwscripts
```

