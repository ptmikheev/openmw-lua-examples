## SkyrimCompat

OpenMW 0.49 has initial support of Oblivion, Fallout3, Fallout NV, Skyrim.
It can load locations, but no quests and no game mechanics.
Many areas in the games are not passable because of non functional scripts and activators.
**SkyrimCompat** is OpenMW mod that disables some objects that block the way and adds a few tweaks in order to allow playing Skyrim in OpenMW as a walking simulator.

In particular this mod was used to make this [video](https://www.youtube.com/watch?v=nUnsT7ectSI)

### How to load Skyrim in OpenMW

Note that Skyrim is needed, Skyrim SE is not supported yet.

1. Get latest OpenMW dev build from [openmw.org](https://openmw.org/downloads/) (Development Builds section).

Optional: alternatively build from source my [experimental branch](https://gitlab.com/ptmikheev/openmw/-/commits/tmp5) that may have some work-in-progress features not yet merged to master.

2. Configure OpenMW to load Morrowind (for details see [OpenMW docs](https://openmw.readthedocs.io/en/latest/))

Optional: if you don't have Morrowind use [OpenMW Example Suite](https://gitlab.com/OpenMW/example-suite/) instead.

3. Install Skyrim as a mod on top of Morrowind:

Add to `openmw.cfg`:

```
data=/home/username/steam/steamapps/common/Skyrim/Data <replace with your path to skyrim data files>

fallback-archive=Skyrim - Misc.bsa
fallback-archive=Skyrim - Shaders.bsa
fallback-archive=Skyrim - Textures.bsa
fallback-archive=Skyrim - Interface.bsa
fallback-archive=Skyrim - Animations.bsa
fallback-archive=Skyrim - Meshes.bsa
fallback-archive=Skyrim - Sounds.bsa
fallback-archive=Skyrim - Voices.bsa
fallback-archive=Skyrim - VoicesExtra.bsa

content=Skyrim.esm
content=Update.esm

# Optional: show BGS logo from Skyrim
fallback=Movies_Company_Logo,BGS_logo.bik

# Optional: Disable Morrowind intro vido
fallback=Movies_New_Game,skip.bik
```

Add to `settings.cfg`:

```
[Models]
load unsupported nif files = true
```

4. (optional) Install any mods you need.

In particular SkyrimCompat. Download from this repo. Then in `openmw.cfg`:

```
data="/path/to/openmw-lua-examples/SkyrimCompat"
content=SkyrimCompat.omwscripts
```

Also you may want to [hide error markers](https://www.nexusmods.com/morrowind/mods/51904) if there are too lot of them (at the moment not all Skyrim objects are loaded correctly).

