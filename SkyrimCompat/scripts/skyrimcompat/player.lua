local types = require('openmw.types')
local self = require('openmw.self')
local ui = require('openmw.ui')
local input = require('openmw.input')

return {
    eventHandlers = {
        onNewGame = function()
            types.NPC.stats.skills.athletics(self).base = 150
            input.setControlSwitch(input.CONTROL_SWITCH.Controls, true)
            input.setControlSwitch(input.CONTROL_SWITCH.Fighting, true)
            input.setControlSwitch(input.CONTROL_SWITCH.Jumping, true)
            input.setControlSwitch(input.CONTROL_SWITCH.Looking, true)
            input.setControlSwitch(input.CONTROL_SWITCH.Magic, true)
            input.setControlSwitch(input.CONTROL_SWITCH.VanityMode, true)
            input.setControlSwitch(input.CONTROL_SWITCH.ViewMode, true)
            eqp = types.Actor.getEquipment(self)
            eqp[types.Actor.EQUIPMENT_SLOT.CarriedRight] = 'iron broadsword'
            types.Actor.setEquipment(self, eqp)
        end,
        ESM4ItemAdded = function(item)
            local itemName = ({
                [types.ESM4Armor] = 'Armor',
                [types.ESM4Clothing] = 'Clothing',
                [types.ESM4Ingredient] = 'Ingredient',
                [types.ESM4Miscellaneous] = 'Item',
                [types.ESM4Potion] = 'Item',
                [types.ESM4Weapon] = 'Weapon',
            })[item.type]
            ui.showMessage(itemName .. ' is added to your inventory')
        end,
    },
}

