local async = require('openmw.async')
local core = require('openmw.core')
local world = require('openmw.world')
local util = require('openmw.util')
local types = require('openmw.types')
local I = require('openmw.interfaces')

if not core.contentFiles.has('Skyrim.esm') then
    return
end

local blacklistObj = {}
local blacklistRecord = {}
local function addObjectToBlackList(formId)
    blacklistObj[world.getObjectByFormId(formId).id] = true
end
local function addRecordToBlackList(formId)
    blacklistRecord[formId] = true
end

-- Helgen
addObjectToBlackList(core.getFormId('Skyrim.esm', 0x1041ae)) -- static object that blocks exit from helgen
addObjectToBlackList(core.getFormId('Skyrim.esm', 0x1041af)) -- static object that blocks exit from helgen
addObjectToBlackList(core.getFormId('Skyrim.esm', 0x25fae))  -- autodoor to helgenkeep01
addObjectToBlackList(core.getFormId('Skyrim.esm', 0x25fb2))  -- autodoor to helgenkeep01
for i = 0x10ab1c, 0x10ab24 do
    addObjectToBlackList(core.getFormId('Skyrim.esm', i))    -- collapsed ceiling in helgenkeep01
end

-- Barricades in Whiterun
addRecordToBlackList(core.getFormId('Skyrim.esm', 0x89fef))
addRecordToBlackList(core.getFormId('Skyrim.esm', 0xb5d66))
addRecordToBlackList(core.getFormId('Skyrim.esm', 0x71e62))
addRecordToBlackList(core.getFormId('Skyrim.esm', 0x5b1e9))
addRecordToBlackList(core.getFormId('Skyrim.esm', 0x5b1cb))

-- Bridge at Whiterun gate; impassable because of incorrect collisions.
-- addObjectToBlackList(core.getFormId('Skyrim.esm', 0x6a167))


local EnableObject = async:registerTimerCallback('EnableObject', function(obj) obj.enabled = true end)

local function ESM4ItemActivation(item, actor)
    if actor.type == types.Player then
        actor:sendEvent('ESM4ItemAdded', item)
    end
    -- Inventory doesn't support ESM4 objects yet
    -- So we just hide the item and respawn in 90 second - it is enough to show a gameplay video ;)
    item.enabled = false
    async:newSimulationTimer(90, EnableObject, item)
end

local function ESM4ActivatorActivation(obj, actor)
    -- Hide activators (e.g.lifting grates 0x6d024) the same way as doors
    obj.enabled = false
    async:newSimulationTimer(5, EnableObject, obj)
end

I.Activation.addHandlerForType(types.ESM4Armor, ESM4ItemActivation)
I.Activation.addHandlerForType(types.ESM4Clothing, ESM4ItemActivation)
I.Activation.addHandlerForType(types.ESM4Ingredient, ESM4ItemActivation)
I.Activation.addHandlerForType(types.ESM4Miscellaneous, ESM4ItemActivation)
I.Activation.addHandlerForType(types.ESM4Potion, ESM4ItemActivation)
I.Activation.addHandlerForType(types.ESM4Weapon, ESM4ItemActivation)
I.Activation.addHandlerForType(types.ESM4Activator, ESM4ActivatorActivation)

return {
    engineHandlers = {
        onNewGame = function()
            local player = world.players[1]
            if player then
                world.createObject('iron broadsword'):moveInto(player)
                player:teleport('helgenexterior', util.vector3(18230, -78541, 8387), util.transform.identity)
                player:sendEvent('onNewGame')
            end
        end,
        onObjectActive = function(obj)
            if blacklistObj[obj.id] or blacklistRecord[obj.recordId] then
                obj.enabled = false
            end
        end,
    },
}

