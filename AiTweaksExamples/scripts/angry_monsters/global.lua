local Creature = require('openmw.types').Creature

local manHuntingCreatures = {
    ['rat'] = true,
    ['cliff racer'] = true,
}

return {
    engineHandlers = {
        onActorActive = function(actor)
            if actor.type == Creature and manHuntingCreatures[actor.recordId] then
                actor:addScript('scripts/angry_monsters/man_hunting.lua')
            end
        end,
    },
}
