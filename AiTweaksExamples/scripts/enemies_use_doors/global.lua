if require('openmw.core').API_REVISION < 22 then
    error('This mod requires a newer version of OpenMW, please update.')
end

local world = require('openmw.world')
local async = require('openmw.async')
local aux_util = require('openmw_aux.util')
local Actor = require('openmw.types').Actor
local Door = require('openmw.types').Door

local teleportWithDelay = async:registerTimerCallback('teleport',
function(data)
    local actor, target = data.actor, data.target
    if not actor:isValid() or actor.cell.name == data.destCellName then
        print('teleportWithDelay: Not valid or already in destCell: ' .. tostring(actor))
        return
    end
    if target:isValid() and target.cell == actor.cell then
        print('teleportWithDelay: Target has returned back')
        return
    end
    print(string.format('Teleport %s to cell=%s pos=%s', actor, data.destCellName, data.destPos))
    actor:teleport(data.destCellName, data.destPos, data.destRot)
end)

return {
    eventHandlers = {
        -- Chase enemies. It searchs for the nearest teleport door that leads to the cell
        -- the enemy is, calculates how much time is needed to run there, waits this time,
        -- and teleports the actor the same way like the door was used.
        InactiveInCombat = function(data)
            local actor, target = unpack(data)
            print(string.format('InactiveInCombat actor=%s target=%s', actor, target))
            if not (actor:isValid() and target:isValid()) then return end
            if actor.cell:isInSameSpace(target) or not Actor.canMove(actor) then return end
            local bestDoor, pathLength = aux_util.findMinScore(actor.cell:getAll(Door), function(door)
                return Door.destCell(door) == target.cell and
                       (actor.position - door.position):length() + (target.position - Door.destPosition(door)):length()
            end)
            if bestDoor == nil then
                print(string.format('Teleport door from "%s" to "%s" not found', actor.cell.name, target.cell.name))
                return
            end
            local delay = (actor.position - bestDoor.position):length() / Actor.runSpeed(actor)
            print(string.format('delay = %f', delay))
            if delay > 15 then return end
            -- TODO: Use pathfinding to verify that the door is reachable and calculate the delay more accurate.
            async:newSimulationTimer(delay, teleportWithDelay, {
                actor = actor,
                target = target,
                destCellName = Door.destCell(bestDoor).name,
                destPos = Door.destPosition(bestDoor),
                destRot = Door.destRotation(bestDoor),
            })
        end
    },
}

