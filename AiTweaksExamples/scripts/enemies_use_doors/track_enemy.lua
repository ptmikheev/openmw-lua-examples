local self = require('openmw.self')
local core = require('openmw.core')
local ai = require('openmw.interfaces').AI

return {
    engineHandlers = {
        -- A cell becomes inactive if player leaves it. If it happens during combat then
        -- likely the enemy was the player or one of his followers, so we need to chase them.
        -- A local script can not change anything while the cell is inactive, so we send an event to global scripts.
        onInactive = function()
            local target = ai.getActiveTarget('Combat') or ai.getActiveTarget('Pursue')
            if target ~= nil then
                core.sendGlobalEvent('InactiveInCombat', {self.object, target})
            end
        end
    },
}

